import java.util.ArrayList;
import java.util.Collections;

public class ArrayDocument {
    //Define attributes
    private String name;
    private ArrayList<Document> documents;

    //Constructor
    public ArrayDocument(String name) {
        this.name = name;
        documents = new ArrayList<>();
       // this.documents = documents;
    }

    //Getter and Setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<Document> documents) {
        this.documents = documents;
    }

    public void sortAscending() {
        Collections.sort(documents, new OrderByDate());
    }

    public void sortDescending() {
        Collections.sort(documents, new OrderByDate().reversed());
    }
}
