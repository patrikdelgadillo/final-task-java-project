import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class MainProgram {
    public static void main(String[] args) {

        //Collection SET
        Set<Document> documents = new HashSet<Document>();

        //Create documents
        Document docOne = new Document("Title One","Content of the document 1");
        Document docTwo = new Document("Title two", "Content of the document 2");
        Document docThree = new Document("Title One", "Content of the document 3");
        Document docFour = new Document("Title four", "Content of the document 4");
        Document docFive = new Document("Title five", "Content of the document 5");

        // Add documents
        documents.add(docOne);
        documents.add(docTwo);
        documents.add(docThree);
        documents.add(docFour);
        documents.add(docFive);

        //Print the content of the document
        System.out.println(documents);


        //Add a new document by keyboard
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please, Enter a Title");
        String title = scanner.nextLine();
        System.out.println("Please, Enter a Content");
        String content = scanner.nextLine();

        Document newDocument = new Document(title, content);
        documents.add(newDocument);

        //Print the Menu Options
        printMainMenu();



        int enterNumber = scanner.nextInt();
        switch (enterNumber) {
            case 1:
                System.out.println(documents);
                break;
            case 2:
                //Collections.sort(Document, new OrderByDate());
                break;
            case 3:
                //Collections.sort(Document, new OrderByDate().reversed());
                break;
            case 4:
                System.out.println(documents);
                break;

            default:
                new Exception("The option isn't valid");
        }


    }

    private static void printMainMenu() {
        System.out.println("=======================================================================================");
        System.out.println("==========                            MENU OPTIONS                           ==========");
        System.out.println("=======================================================================================");
        System.out.println("1 - Show document content");
        System.out.println("2 - Sort by creation date, ascending");
        System.out.println("3 - Sort by creation date, descending");
        System.out.println("4 - List of documents");
        System.out.println("5 - Translate Document ** Not Available **");
        System.out.println("=======================================================================================");
        System.out.print("Please enter an integer number: ");
    }

}
