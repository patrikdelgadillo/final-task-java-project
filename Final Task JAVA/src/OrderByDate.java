import java.util.Comparator;

public class OrderByDate implements Comparator <Document> {
    public int compare (Document one, Document two){
        return one.getDateOfCreation().compareTo(two.getDateOfCreation());
    }
}




//Example
//https://www.geeksforgeeks.org/comparator-interface-java/
/*class Sortbyname implements Comparator<Student>
{
    // Used for sorting in ascending order of
    // roll name
    public int compare(Student a, Student b)
    {
        return a.name.compareTo(b.name);
    }
}*/
//endregion