import java.util.Date;
import java.util.Objects;

public class Document {
    //Define attrinutes
    private String title;
    private int amountChars;
    private Date dateOfCreation;
    private String contentDocument;

    //generate constructor
    public Document(String title, String contentDocument) {
        this.title = title;
        this.amountChars = contentDocument.length();
        this.dateOfCreation = new Date();
        this.contentDocument = contentDocument;
    }

    // Region Getter and Setter
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAmountChars() {
        return amountChars;
    }

    public void setAmountChars(int amountChars) {
        this.amountChars = amountChars;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getContentDocument() {
        return contentDocument;
    }

    public void setContentDocument(String contentDocument) {
        this.contentDocument = contentDocument;
    }
    //endregion

    @Override
    public String toString() {
        return "Docuemnt title= " + getTitle()  + ", Amount Chars= " + getAmountChars() +
                ", Date of Creation= " + getDateOfCreation()  +"\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return Objects.equals(title, document.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
